var express = require(`express`);
var fs = require(`fs`);
var bodyParser = require("body-parser");
var hbs = require(`hbs`);
var mongoClient = require("mongodb").MongoClient;
var objectId = require("mongodb").ObjectID;
var sha512 = require('sha512');
var logger = require('morgan');
var cookieParser = require('cookie-parser')

var url = "mongodb://localhost:27017/usersdb";

var app = express();

//создадим парсер для данных application/x-www-form-urlencoded
var urlencodedParser = bodyParser.urlencoded({extended: false});
//парсер для данных в форммате json
var jsonParser = bodyParser.json();

// устанавливаем путь к каталогу с частичными представлениями
hbs.registerPartials(__dirname + "/views/partials");

//установим  Handlebars в качестве движка представлений
app.set("view engine", "hbs");

//запишем в лог историю запросов к серверу в файл
app.use(function(request,response,next){
    var now = new Date();
    var hour = now.getHours();
    var minutes = now.getMinutes();
    var seconds = now.getSeconds();
    var data = `${hour}:${minutes}:${seconds} ${request.method} ${request.url} ${request.get("user-agent")}`;
    fs.appendFile(`server.log`, data + "\n",  (err) => {
        if (err) throw err;
      });
    next();
});

app.use(logger('dev'));

app.use(cookieParser());
 
//добавим middleware в конвеер обработки запросов
//в котором укажим путь к папке со статическими файлами
app.use(express.static(__dirname +  "/public"));


app.get("/", function(request, response){
    response.render("index.hbs");
});

app.get("/index.html", function(request, response){
    response.render("index.hbs");
    console.log('Cookies: ', request.cookies);    
});

app.get("/about.html", function(request, response){
    response.render("about.hbs");
});

app.get("/contacts.html", function(request,response){
    //сделаем рендеринг представления "contact.hbs" 
    response.render("contacts.hbs",{
        title: "Контакты",
        emailsVisible: true,
        emails: ["email@corp.ru", "admin@corp.ru"],
        phone: "+7(xxx)xxx-xx-xx"
    });
});

app.get("/register.html", function(request, response){
    response.render("register.hbs");
});

//вернем все документы из бд в коллекции users
app.get("/api/users", function(req, res){

    mongoClient.connect(url, function(err, db){
        db.collection("users").find({}).toArray(function(err, users){
            res.send(users)
            db.close();
        });
    });
});

//получим из бд одного юзера по его id
app.get("/api/users/:id", function(req, res){
     var id = new objectId(req.params.id);
     mongoClient.connect(url, function(err, db){
         db.collection("users").findOne({_id: id}, function(err, user){

            if(err) return res.status(400).send();

            res.send(user);

            db.close();
         });
     });
});

function testInjection(name){
    var exp = /^[a-zA-Z-яА-Я0-9а@_.-]+$/;
    return exp.test(name);
}
//добавим нового юзера
app.post("/api/users/", jsonParser, function(req, res){
    if(!req.body) return res.sendStatus(400);

    var userEmail = req.body.email;
    var userName = req.body.name;
    if(!testInjection(userEmail)||!testInjection(userName)) 
        return res.sendStatus(400);

    var userPass = sha512(req.body.pass);
    var user = {email: userEmail, name: userName, pass: userPass.toString("hex")};

    mongoClient.connect(url, function(err, db){

        db.collection("users").insertOne(user, function(err, result){

            if(err) return res.status(400).send();

            res.send(user);
            db.close();
        });
    });
});


//вход в аккаунт
app.post("/api/users/login", jsonParser, function(req, res){
    if(!req.body) return res.sendStatus(400);

    var userEmail = req.body.email;
    if(!testInjection(userEmail)) 
        return res.sendStatus(400);

    var userPass = sha512(req.body.pass);
    var user = {email: userEmail, pass: userPass.toString('hex')};


    console.log(user);
    mongoClient.connect(url, function(err, db){
        db.collection("users").findOne(user, function(err, doc){
            if(err) return console.log(err);
            if(doc == null) return res.send(null);
            var user = { email: doc.email, name: doc.name};
            console.log(user);

            //var randomNumber=Math.random().toString();
            //randomNumber=randomNumber.substring(2,randomNumber.length);
            //response.cookie('userToken',randomNumber, { maxAge: 43200000, httpOnly: true });

            res.send(user);

            db.close();

        });
    });
});

///удалим пользователя по id

app.delete("/api/users/:id", function(req, res){
    var id = new objectId(req.params.id);
    mongoClient.connect(url, function(err, db){
        db.collection("users").findOneAndDelete({_id: id}, function(err, result){
            
            if(err) return res.status(400).send();

            var user = result.value;
            res.send(user);
            db.close();
        });
    });
});


//обновим юзера по его id
app.put("/api/users", jsonParser, function(req, res){
    
    if(!req.body) return res.status(400).send();
    var id = new objectId(req.body.id);
    var userEmail = req.body.email;
    var userName = req.body.name;
    var userPass = sha512(req.body.pass);

    mongoClient.connect(url, function(err, db){
        db.collection("users").findOneAndUpdate({_id: id}, { $set: {pass: userPass, name: userName.toString("hex"), email: userEmail}},
             {returnOriginal: false },function(err, result){

            if(err) return res.status(400).send();

            var user = result.value;
            res.send(user);
            db.close();
        });
    });
});






app.listen(3000);